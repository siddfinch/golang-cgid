package main

import (
	"flag"
	"log"
	"net/http"
	"net/http/cgi"
	"os"
)

var cmd = flag.String("cmd", "", "CGI program to run.")
var wd = flag.String("wd", "", "Working directory for CGI.")
var debug = flag.Bool("debug", false, "Print debug messages to stderr.")
var address = flag.String("address", ":9991", "Listen address.")

func main() {

	flag.Usage = usage
	flag.Parse()

	if *cmd == "" {
		usage()
	}

	c := *cmd
	if c[0] != '/' {
		c = "./" + c
	}

	os.Setenv("PATH", os.Getenv("PATH")+":.")

	cgiHandler := &cgi.Handler{
		Path:       c,
		Root:       "/",
		Dir:        *wd,
		InheritEnv: []string{"PATH", "PLAN9"},
	}

	var err error
	log.Println("Starting HTTP server listening on ", *address)
	err = http.ListenAndServe(*address, cgiHandler)

	if err != nil {
		log.Fatal(err)
	}

}

func usage() {
	os.Stderr.WriteString("usage: cgid -cmd prog [-address addr] [-wd working dir] [-debug]\n")
	flag.PrintDefaults()
	os.Exit(2)
}
