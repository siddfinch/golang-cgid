# golang-cgid

Based off of  https://github.com/uriel/cgd thiis a http server that only runs 
cgi scripts, via golang. 

```
The objective it so do something like:
$ curl -X DELETE http://localhost:9991/blah 

To create small rest api/crud type services for legacy apps and code. 
```

```
$ make build
$ mkdir /tmp/bofh
$ ./cgid -wd /tmp/both -cmd `pwd`/cgi/bofh-cgi.pl
2019/11/15 14:33:16 Starting HTTP server listening on  :9991
```
