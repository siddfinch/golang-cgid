default: clean build 

fmt: main.go
	@go fmt

build: fmt main.go
	@go build 

clean:
	@rm -f `cat TARGETS`
	@rm -f *~

%-build: main.go cgi/bofh-cgi.pl
	@docker build -t $* -f Dockerfile.$*  .

%-run:
	@docker run --rm -p 9991:9991 --name $* -d $*:latest

%-stop:
	@docker stop $*
	
